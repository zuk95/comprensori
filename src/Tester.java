import comprensori.Comprensorio;
import comprensori.ImpiantoDiRisalita;
import skipass.AbstractSkipass;
import skipass.CoppiaDate;
import skipass.TypeSkipass;

public class Tester {
    public static void main(String[] args) {
        Comprensorio comprensorio = new Comprensorio("ccc","Prova");
        Comprensorio comprensorio1 = new Comprensorio("sss","altro");


        ImpiantoDiRisalita impiantoDiRisalita = new ImpiantoDiRisalita("primo",10);
        ImpiantoDiRisalita impiantoDiRisalita1 = new ImpiantoDiRisalita("secondo",100);
        ImpiantoDiRisalita impiantoDiRisalita2 = new ImpiantoDiRisalita("terzo",500);
        ImpiantoDiRisalita impiantoDiRisalita3 = new ImpiantoDiRisalita("quarto",300);
        ImpiantoDiRisalita impiantoDiRisalita4 = new ImpiantoDiRisalita("cinque",30);

        comprensorio.addImpianto(impiantoDiRisalita);
        comprensorio.addImpianto(impiantoDiRisalita1);
        comprensorio.addImpianto(impiantoDiRisalita2);
        comprensorio.addImpianto(impiantoDiRisalita3);

        comprensorio1.addImpianto(impiantoDiRisalita1);
        comprensorio1.addImpianto(impiantoDiRisalita2);


        AbstractSkipass stagionale= comprensorio.rilascioSkipass(TypeSkipass.STAGIONALE,0,3,1,2000,2,1,2000);
        //AbstractSkipass ricaricabile = comprensorio1.rilascioSkipass(TypeSkipass.RICARICABILE,800);
        //AbstractSkipass giornaliero = comprensorio.rilascioSkipass(TypeSkipass.GIORNALIERO,0);

        //stagionale a comprensorio, ricaricabile a comprensorio1, giornaliero a comprensorio


        impiantoDiRisalita.accesso(stagionale);//true
        //impiantoDiRisalita1.accesso(ricaricabile);//true
        //impiantoDiRisalita4.accesso(giornaliero);//false


        System.out.println(comprensorio);
        System.out.println(comprensorio1);

        CoppiaDate coppiaDate = new CoppiaDate(1,1,2018,2,1,2018);
        boolean c = coppiaDate.controlloValiditaDate(); //controllo se funziona questa funzionalita

        AbstractSkipass daRiga = comprensorio.rilascioSkipass();
        System.out.println(daRiga);



    }
}
