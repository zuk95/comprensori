package skipass;

import eccezioni.ArgNotValidException;
import sun.util.resources.cldr.CalendarData;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class CoppiaDate {
    private Calendar dataInizio;
    private Calendar dataScadenza;

    public CoppiaDate(int giornoI,int meseI,int annoI,int giornoF,int meseF,int annoF) {
        try {
            this.dataInizio = new GregorianCalendar();
            this.dataScadenza = new GregorianCalendar();
            this.dataInizio.set(annoI, meseI, giornoI);
            this.dataScadenza.set(annoF, meseF, giornoF);
            if(dataScadenza.before(dataInizio) || dataScadenza.equals(dataInizio)){
                throw new ArgNotValidException();
            }
        }catch (ArgNotValidException a){
            System.err.println(a.getMessage());
        }
    }

    public boolean controlloValiditaDate(){
        return dataScadenza.after(dataInizio);
    }

}
