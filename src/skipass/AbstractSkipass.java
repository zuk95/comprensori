package skipass;

import comprensori.Comprensorio;
import comprensori.ImpiantoDiRisalita;

public abstract class AbstractSkipass {
    private TypeSkipass tipo;
    private String codice;
    private Comprensorio comprensorioValido;
    private CoppiaDate date;

    public AbstractSkipass(TypeSkipass tipo,int giornoI,int meseI,int annoI,int giornoF,int meseF,int annoF) {
        this.tipo = tipo;
        this.date=new CoppiaDate(giornoI, meseI, annoI, giornoF, meseF, annoF);
    }

    public boolean controlloDate(){
        return date.controlloValiditaDate();
    }

    public String visualizzaValidita(){
        if(controlloDate()){
            return "VALIDO";
        }else {
            return "SCADUTO";
        }
    }

    public void setCodice(String codice) {
        this.codice = codice;
    }

    public CoppiaDate getDate() {
        return date;
    }

    public void setComprensorioValido(Comprensorio comprensorioValido) {
        this.comprensorioValido = comprensorioValido;
    }

    public Comprensorio getComprensorioValido() {
        return comprensorioValido;
    }

    public abstract boolean accessoValido(ImpiantoDiRisalita impianto,CoppiaDate date);


    @Override
    public String toString() {
        return "AbstractSkipass{" +
                "tipo=" + tipo +
                ", "+ visualizzaValidita()+
                '}';
    }
}
