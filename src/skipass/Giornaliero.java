package skipass;

import comprensori.ImpiantoDiRisalita;
import eccezioni.ArgNotValidException;

public class Giornaliero extends AbstractSkipass{

    public Giornaliero(TypeSkipass tipo, int giornoI, int meseI, int annoI, int giornoF, int meseF, int annoF) {
            super(tipo, giornoI, meseI, annoI, giornoF, meseF, annoF);
            if(giornoF!=(giornoI+1) || meseF!=meseI || annoF!=annoI){
                throw new ArgNotValidException();
            }
    }

    @Override
    public boolean accessoValido(ImpiantoDiRisalita impianto,CoppiaDate date) {
        return super.controlloDate();
    }
}
