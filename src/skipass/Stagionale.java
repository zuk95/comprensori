package skipass;

import comprensori.ImpiantoDiRisalita;

public class Stagionale extends AbstractSkipass {
    public Stagionale(TypeSkipass tipo, int giornoI, int meseI, int annoI, int giornoF, int meseF, int annoF) {
        super(tipo, giornoI, meseI, annoI, giornoF, meseF, annoF);
    }

    @Override
    public boolean accessoValido(ImpiantoDiRisalita impianto,CoppiaDate date) {
        return super.controlloDate();
    }
}
