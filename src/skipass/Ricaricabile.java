package skipass;

import comprensori.ImpiantoDiRisalita;

public class Ricaricabile extends AbstractSkipass{
    private int punti;

    public Ricaricabile(TypeSkipass tipo, int giornoI, int meseI, int annoI, int giornoF, int meseF, int annoF, int punti) {
        super(tipo, giornoI, meseI, annoI, giornoF, meseF, annoF);
        this.punti = punti;
    }

    public int ricarica(int ricarica){
        return punti+ricarica;
    }

    @Override
    public boolean accessoValido(ImpiantoDiRisalita impianto,CoppiaDate date) {
        if(this.punti>impianto.getCostoUtilizzo() && super.controlloDate()){
            this.punti = this.punti-impianto.getCostoUtilizzo();
            return true;
        }
        return false;
    }
}
