package eccezioni;

import java.util.InputMismatchException;

public class ArgNotValidException extends InputMismatchException {
    public ArgNotValidException() {
        super(("ARGOMENTO NON VALIDO"));
    }
}
