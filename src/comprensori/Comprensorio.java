package comprensori;

import eccezioni.ArgNotValidException;
import skipass.*;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Comprensorio {
    private ArrayList<ImpiantoDiRisalita> impianti;
    private String codice,nome;
    private int i =0;

    public Comprensorio(String codice, String nome) {
        this.codice = codice;
        this.nome = nome;
        this.impianti = new ArrayList<>();
    }

    public boolean addImpianto(ImpiantoDiRisalita i){
        return impianti.add(i);
    }

    public ArrayList<ImpiantoDiRisalita> getImpianti() {
        return impianti;
    }

    public AbstractSkipass rilascioSkipass(TypeSkipass tipo,int punti,int giornoI,int meseI,int annoI,int giornoF,int meseF,int annoF){
        AbstractSkipass skipass=null;
        switch (tipo){
            case STAGIONALE: skipass= new Stagionale(tipo, giornoI, meseI, annoI, giornoF, meseF,annoF);
            break;

            case RICARICABILE: skipass= new Ricaricabile(tipo,punti, giornoI, meseI, annoI, giornoF, meseF,annoF);
            break;

            case GIORNALIERO: skipass = new Giornaliero(tipo, giornoI, meseI, annoI, giornoF, meseF,annoF);
            break;
        }

        skipass.setCodice(generacodice());
        skipass.setComprensorioValido(this);
        return skipass;
    }

    public AbstractSkipass rilascioSkipass(){
            AbstractSkipass skipass = null;

try {
    Scanner s = new Scanner(System.in);
    TypeSkipass t = null;
    System.out.println("INSERISCI TIPO: ");
    String tipo = s.next();
    switch (tipo) {
        case "STAGIONALE":
            t = TypeSkipass.STAGIONALE;
            skipass = new Stagionale(t, s.nextInt(), s.nextInt(), s.nextInt(), s.nextInt(), s.nextInt(), s.nextInt());
            break;

        case "RICARICABILE":
            t = TypeSkipass.RICARICABILE;
            skipass = new Ricaricabile(t, s.nextInt(), s.nextInt(), s.nextInt(), s.nextInt(), s.nextInt(), s.nextInt(), s.nextInt());
            break;

        case "GIORNALIERO":
            t = TypeSkipass.GIORNALIERO;
            skipass = new Giornaliero(t, s.nextInt(), s.nextInt(), s.nextInt(), s.nextInt(), s.nextInt(), s.nextInt());
            break;
    }

    skipass.setCodice(generacodice());
    skipass.setComprensorioValido(this);
}catch (NullPointerException e){
    System.err.println(e.getMessage());
}catch (ArgNotValidException a){
    System.err.println(a.getMessage());
}catch(InputMismatchException e){
    System.err.println(e.getMessage());
}
            return skipass;
    }


    private String generacodice(){
        return nome+(i+1)+codice;
    }

    @Override
    public String toString() {
        return "Comprensorio{" +
                "impianti=" + impianti +
                ", nome='" + nome + '\'' +
                '}';
    }
}
