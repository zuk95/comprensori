package comprensori;

import skipass.AbstractSkipass;

import java.util.ArrayList;

public class ImpiantoDiRisalita {
    private ArrayList<AbstractSkipass> skipassRegistrati;
    private String nome;
    private int posizione,costoUtilizzo,numAccessi;

    public ImpiantoDiRisalita(String nome, int posizione) {
        this.skipassRegistrati=new ArrayList<>();
        this.nome = nome;
        this.posizione = posizione;
        this.costoUtilizzo=calcoloCosto();
    }

    private int calcoloCosto(){
        return this.posizione*2;
    }

    public boolean faParteDiQuestoComprensorio(Comprensorio c){
        return c.getImpianti().contains(this);
    }


    public boolean accesso(AbstractSkipass skipass){
        if(faParteDiQuestoComprensorio(skipass.getComprensorioValido()) && skipass.accessoValido(this,skipass.getDate())){
            this.numAccessi++;
            return skipassRegistrati.add(skipass);
        }
        return false;
    }


    public int getCostoUtilizzo() {
        return costoUtilizzo;
    }

    @Override
    public String toString() {
        return "ImpiantoDiRisalita{" +
                "skipassRegistrati=" + skipassRegistrati +
                ", nome='" + nome + '\'' +
                ", posizione=" + posizione +
                ", costoUtilizzo=" + costoUtilizzo +
                ", numAccessi=" + numAccessi +
                '}';
    }
}
